extends Node2D

signal plugged_in(port)
signal unplugged(port)


var port


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Plug_plugged_in(port_):
	emit_signal("plugged_in", port_.get_parent())
	port = port_.get_parent()


func _on_Plug_unplugged(port_):
	emit_signal("unplugged", port_.get_parent())
	port = null
