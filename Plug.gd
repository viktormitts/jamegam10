extends Area2D

signal dragged(position)
signal plugged_in(port)
signal unplugged(port)


var dragging = false
var near_port = null
var plugged_in = false


func _on_Plug_input_event(_viewport, event, _shape_idx):
	if event.is_action_pressed("grab"):
		dragging = true


func _input(event):
	if event is InputEventMouseMotion and dragging:
		if near_port == null:
			global_position = event.position
			emit_signal("dragged", position)
		elif (event.position - global_position).length() > 40:
			if plugged_in:
				plugged_in = false
				$PlugIn.visible = false
				$PlugOut.visible = true
				emit_signal("unplugged", near_port)
				$UnplugSound.play()
			near_port = null
	elif event.is_action_released("grab") and dragging:
		dragging = false
		if near_port != null:
			emit_signal("plugged_in", near_port)
			plugged_in = true
			$PlugIn.visible = true
			$PlugOut.visible = false
			$PlugInSound.play()


func _on_Plug_area_entered(area):
	near_port = area
	global_position = area.global_position
	emit_signal("dragged", position)
