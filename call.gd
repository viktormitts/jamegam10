extends Reference

class_name Call

# Port that the call comes from.
var caller
# Recipient port of the call.
var recipient
# Record name of the dialog.
var dialog: String
# Duration of the call in seconds.
var duration: float


func _init(caller_, recipient_, dialog_: String, duration_: float):
	caller = caller_
	recipient = recipient_
	dialog = dialog_
	duration = duration_
