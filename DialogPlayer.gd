extends Node

signal dialog_finished(did)

onready var _Body_AnimationPlayer = find_node("Body_AnimationPlayer")
onready var _Body_Label = find_node("Body_Label")
onready var _DialogBox = find_node("DialogBox")
onready var _Speaker_Label = find_node("Speaker_Label")
onready var _Prompt_Icon = find_node("Prompt_NinePatchRect")

var _did = 0
var _nid = 0
var _final_nid = 0
var _story_reader


# Called when the node enters the scene tree for the first time.
func _ready():
	var story_reader_class = load("res://addons/EXP-System-Dialog/Reference_StoryReader/EXP_StoryReader.gd")
	_story_reader = story_reader_class.new()
	
	var story = load("res://story_baked.tres")
	_story_reader.read(story)
	
	_DialogBox.visible = false
	_Prompt_Icon.visible = false


func _input(event):
	if event.is_action_pressed("ui_accept") and _is_waiting():
		_Prompt_Icon.visible = false
		_get_next_node()
		if _is_playing():
			_play_node()

func _on_Body_AnimationPlayer_animation_finished(_anim_name):
	_Prompt_Icon.visible = true


func play_dialog(record_name: String):
	_did = _story_reader.get_did_via_record_name(record_name)
	_nid = _story_reader.get_nid_via_exact_text(_did, "<start>")
	_final_nid = _story_reader.get_nid_via_exact_text(_did, "<end>")
	_get_next_node()
	_play_node()
	_DialogBox.visible = true

func _is_playing() -> bool:
	return _DialogBox.visible


func _is_waiting() -> bool:
	return _Prompt_Icon.visible


func _get_next_node():
	_nid = _story_reader.get_nid_from_slot(_did, _nid, 0)
	
	if _nid == _final_nid:
		_DialogBox.visible = false
		emit_signal("dialog_finished", _did)


func _get_tagged_text(tag: String, text: String) -> String:
	var start_tag = "<" + tag + ">"
	var end_tag = "</" + tag + ">"
	var start_index = text.find(start_tag) + start_tag.length()
	var end_index = text.find(end_tag)
	var substr_length = end_index - start_index
	return text.substr(start_index, substr_length)


func _play_node():
	var text = _story_reader.get_text(_did, _nid)
	var speaker = _get_tagged_text("speaker", text)
	var dialog = _get_tagged_text("dialog", text)
	
	_Speaker_Label.text = speaker
	_Body_Label.text = dialog
	_Body_AnimationPlayer.play("TextDisplay")
