extends Timer

signal call_timeout(call)


var call


func _on_CallTimer_timeout():
	emit_signal("call_timeout", call)
	queue_free()
