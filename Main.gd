extends Node2D


var CallTimer

var progress = -100
var whenever_calls
var sequential_calls
var ports = []
var tutorial_call
var physics1done = false


# Called when the node enters the scene tree for the first time.
func _ready():
	CallTimer = preload("res://CallTimer.tscn")
	
	var CablePair = preload("res://CablePair.tscn")
	var cable_y = 715
	var cable_x = 520
	for i in range(cable_x, 1400, 40):
		var cable = CablePair.instance()
		add_child(cable)
		cable.position = Vector2(i, cable_y)
		cable.connect("plugged_in", self, "_on_CablePair_plugged_in")
		cable.connect("connected", self, "_on_CablePair_connected")
	
	var Port = preload("res://PortAndLight.tscn")
	var port_y = 96
	var port_x = 534
	var index = 1
	for j in range(port_y, 586, 130):
		for i in range(port_x, 1386, 60):
			var port = Port.instance()
			add_child(port)
			port.position = Vector2(i, j)
			port.get_node("Label").text = index as String
			ports.append(port)
			index += 1
	
	var Call = load("res://call.gd")
	tutorial_call = Call.new(ports[50], ports[47], "Tutorial/Caller1", 20)
	whenever_calls = [
		[Call.new(ports[43], ports[52], "Normal/1", 600), 2],
		[Call.new(ports[58], ports[9], "Normal/2", 60), 10],
		[Call.new(ports[49], ports[32], "Normal/3", 30), 20],
		[Call.new(ports[10], ports[37], "Normal/4", 120), 25],
		[Call.new(ports[55], ports[27], "Normal/5", 300), 35],
		[Call.new(ports[42], ports[41], "Normal/6", 60), 45],
		[Call.new(ports[35], ports[7], "Normal/7", 60), 50],
	]
	sequential_calls = [
		[Call.new(ports[19], ports[44], "Milton/1", 40), 15],
		[Call.new(ports[4], ports[14], "Warren/1", 60), 10],
		[Call.new(ports[2], ports[20], "Stranger/1", 10), 7],
		[Call.new(ports[19], ports[44], "Milton/2", 35), 15],
		[Call.new(ports[19], ports[44], "Milton/3", 30), 5],
		[Call.new(ports[20], ports[2], "Physics/1", 5), 10], # this one shouldn't actually have a valid destination
		[Call.new(ports[19], ports[44], "Milton/4", 20), 5],
	]
	$DialogPlayer.play_dialog("Tutorial/Boss1")


func _on_DialogPlayer_dialog_finished(did):
	match did:
		# Tutorial
		1:
			# for now, we're just making the one port go off
			tutorial_call.caller.start_call(tutorial_call)
			progress = -90
		6:
			$DialogPlayer.play_dialog("Tutorial/Boss2")
		5:
			# Tutorial over!
			progress = 0
			for call in whenever_calls:
				queue_call(call[0], call[1])
		2:
			if $Phonebook.current_page == 4:
				$DialogPlayer.play_dialog("Tutorial/Boss3")
				progress = -80
		# Not tutorial
		18:
#			if $Phonebook.current_page == 1:
#				$DialogPlayer.play_dialog("Physics/2")
#				_on_CallTimer_call_ended(sequential_calls[5][0])
			physics1done = true
		20:
			$DialogPlayer.play_dialog("Boss/1")


func _on_Phonebook_page_turned(page):
	if progress == -85 and page == 4:
		$DialogPlayer.play_dialog("Tutorial/Boss3")
		progress = -80
	if physics1done and page == 1:
		physics1done = false
		$DialogPlayer.play_dialog("Physics/2")
		_on_CallTimer_call_ended(sequential_calls[5][0])
		


func queue_call(call, delay):
	var timer = CallTimer.instance()
	timer.wait_time = delay
	timer.call = call
	add_child(timer)
	timer.connect("call_timeout", self, "_on_CallTimer_call_starting")
	timer.start()


func _on_CallTimer_call_ended(call):
	call.caller.end_call()
	if not call.dialog.begins_with("Normal"):
		queue_call(sequential_calls[progress][0], sequential_calls[progress][1])
		progress += 1


func _on_CallTimer_call_starting(call):
	call.caller.start_call(call)


func _on_CablePair_plugged_in(_cable, port):
	if port.call:
		$DialogPlayer.play_dialog(port.call.dialog)
		if progress == -90:
			progress = -85
	if progress == -80 and port == tutorial_call.recipient:
		$DialogPlayer.play_dialog("Tutorial/Boss4")
		progress = -70


func _on_CablePair_connected(port1, port2):
	var is_call = false
	var call
	if port1.call and port2 == port1.call.recipient:
		is_call = true
		call = port1.call
	elif port2.call and port1 == port2.call.recipient:
		is_call = true
		call = port2.call
	if is_call:
		if progress == -70:
			$DialogPlayer.play_dialog("Tutorial/Boss5")
		var timer = CallTimer.instance()
		timer.wait_time = call.duration
		timer.call = call
		add_child(timer)
		timer.connect("call_timeout", self, "_on_CallTimer_call_ended")
		timer.start()
