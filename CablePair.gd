extends Node2D

signal connected(port1, port2)
signal plugged_in(cable, port)
signal unplugged(cable, port)


func _on_Cable_plugged_in(port):
	emit_signal("plugged_in", $Cable, port)
#	if port.call:
#		call = port.call
#		in_cable = $Cable


func _on_Cable_unplugged(port):
	emit_signal("unplugged", $Cable, port)
#	if in_cable == $Cable:
#		call = null
#		in_cable = null


func _on_Cable2_plugged_in(port):
	emit_signal("plugged_in", $Cable2, port)
#	if port.call:
#		call = port.call
#		in_cable = $Cable2


func _on_Cable2_unplugged(port):
	emit_signal("unplugged", $Cable2, port)
#	if in_cable == $Cable:
#		call = null
#		in_cable = null


func _on_Switch_input_event(_viewport, event, _shape_idx):
	if event.is_action_pressed("grab") and $Cable.port and $Cable2.port:
		emit_signal("connected", $Cable.port, $Cable2.port)
