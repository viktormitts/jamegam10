extends Node2D


var call


func start_call(incoming_call):
	call = incoming_call
	$Light2D.enabled = true

func end_call():
	call = null
	$Light2D.enabled = false
