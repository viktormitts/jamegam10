extends Node2D

signal book_opened
signal page_turned(page)

onready var pages = $Control/Page/MarginContainer.get_children()

# Declare member variables here. Examples:
var speed = 1200
var open = false
var current_page = 0


func _on_OpenClose_pressed():
	$AudioBookSlide.play()
	if open == false:
		$Control.rect_position += Vector2(500, 0)
		open = true
	else:
		$Control.rect_position -= Vector2(500, 0)
		open = false


func turn_page(page):
	pages[current_page - 1].visible = false
	pages[page - 1].visible = true
	emit_signal("page_turned", page)
	current_page = page


func _on_CoverButton_pressed():
	emit_signal("book_opened")
	turn_page(1)


func _on_ButtonTab1_pressed():
	turn_page(1)
	emit_signal("page_turned", 1)


func _on_ButtonTab2_pressed():
	turn_page(2)
	emit_signal("page_turned", 2)


func _on_ButtonTab3_pressed():
	turn_page(3)
	emit_signal("page_turned", 3)


func _on_ButtonTab4_pressed():
	turn_page(4)
	emit_signal("page_turned", 4)


func _on_ButtonTab5_pressed():
	turn_page(5)
	emit_signal("page_turned", 5)
